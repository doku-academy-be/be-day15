package com.example.shop.controller;

import com.example.shop.entities.Products;
import com.example.shop.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/shop/v1/products")
public class ProductsController {
    @Autowired
    private ProductService services;

    @GetMapping
    public ResponseEntity<List<Products>> getAllProducts() {
        return ResponseEntity.ok(services.getProductList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Products> getProductById(@PathVariable Long id) {
        return ResponseEntity.ok().body(this.services.getProductById(id));
    }

    @PostMapping
    public ResponseEntity<Products> addUser(@RequestBody Products product) {
        return ResponseEntity.ok(this.services.createProduct(product));
    }



    @PutMapping
    public ResponseEntity<Products> updateProduct(@RequestBody Products product) {
        return ResponseEntity.ok().body(this.services.updateProductById(product));
    }

    @DeleteMapping("/{id}")
    public HttpStatus deleteProduct(@PathVariable Long id) {
        this.services.deleteProductById(id);
        return HttpStatus.OK;
    }
}
