package com.example.shop.controller;

import com.example.shop.entities.ProductDetails;
import com.example.shop.entities.Products;
import com.example.shop.services.ProductDetailService;
import com.example.shop.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/shop/v1/productdetail")
public class ProductDetailController {
    @Autowired
    private ProductDetailService services;

    @GetMapping
    public ResponseEntity<List<ProductDetails>> getAllProductDetails() {
        return ResponseEntity.ok(services.getPDList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductDetails> getProductDetailsById(@PathVariable Long id) {
        return ResponseEntity.ok().body(this.services.getPDById(id));
    }


    @PostMapping
    public ResponseEntity<List<ProductDetails>> addProductDetails(@RequestBody List<ProductDetails> list) {
        return ResponseEntity.ok(this.services.createPDList(list));
    }

    @PutMapping
    public ResponseEntity<ProductDetails> updateProductDetail(@RequestBody ProductDetails pd) {
        return ResponseEntity.ok().body(this.services.updatePDById(pd));
    }

    @DeleteMapping("/{id}")
    public HttpStatus deleteProductDetail(@PathVariable Long id) {
        this.services.deletePDById(id);
        return HttpStatus.OK;
    }
}
