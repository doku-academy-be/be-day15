package com.example.shop.controller;

import com.example.shop.entities.Orders;
import com.example.shop.entities.ProductDetails;
import com.example.shop.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/shop/v1/order")
public class OrdersController {
    @Autowired
    private OrderService services;

    @GetMapping
    public ResponseEntity<List<Orders>> getAllOrders() {
        return ResponseEntity.ok(services.getOrderList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Orders> getOrdersById(@PathVariable Long id) {
        return ResponseEntity.ok().body(this.services.getOrderById(id));
    }

    @PostMapping
    public ResponseEntity<Orders> addOrder(@RequestBody Orders order) {
        return ResponseEntity.ok(this.services.createOrder(order));
    }



    @PutMapping
    public ResponseEntity<Orders> updateOrder(@RequestBody Orders order) {
        return ResponseEntity.ok().body(this.services.updatePDById(order));
    }

    @DeleteMapping("/{id}")
    public HttpStatus deleteOrder(@PathVariable Long id) {
        this.services.deleteOrderById(id);
        return HttpStatus.OK;
    }
}
