package com.example.shop.controller;

import com.example.shop.entities.OrderDetails;
import com.example.shop.entities.Orders;
import com.example.shop.services.OrderDetailService;
import com.example.shop.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/shop/v1/orderdetail")
public class OrderDetailController {
    @Autowired
    private OrderDetailService services;

    @GetMapping
    public ResponseEntity<List<OrderDetails>> getAllOrderDetails() {
        return ResponseEntity.ok(services.getODList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<OrderDetails> getOrderDetailsById(@PathVariable Long id) {
        return ResponseEntity.ok().body(this.services.getODById(id));
    }

    @PostMapping
    public ResponseEntity<OrderDetails> addOrderDetail(@RequestBody OrderDetails od) {
        return ResponseEntity.ok(this.services.createOD(od));
    }



    @PutMapping
    public ResponseEntity<OrderDetails> updateOrderDetail(@RequestBody OrderDetails od) {
        return ResponseEntity.ok().body(this.services.updateODById(od));
    }

    @DeleteMapping("/{id}")
    public HttpStatus deleteOrderDetail(@PathVariable Long id) {
        this.services.deleteODById(id);
        return HttpStatus.OK;
    }
}
