package com.example.shop.controller;

import com.example.shop.entities.Users;
import com.example.shop.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/shop/v1/user")
public class UserController {
    @Autowired
    private UserService services;

    @GetMapping
    public ResponseEntity<List<Users>> getAllUsers() {
        return ResponseEntity.ok(services.getUserList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Users> getUserById(@PathVariable Long id) {
        return ResponseEntity.ok().body(this.services.getUserById(id));
    }

    @PostMapping
    public ResponseEntity<Users> addUser(@RequestBody Users user) {
        return ResponseEntity.ok(this.services.createUser(user));
    }



    @PutMapping
    public ResponseEntity<Users> updateUser(@RequestBody Users user) {
        return ResponseEntity.ok().body(this.services.updateUserById(user));
    }

    @DeleteMapping("/{id}")
    public HttpStatus deleteUser(@PathVariable Long id) {
        this.services.deleteUserById(id);
        return HttpStatus.OK;
    }
}
