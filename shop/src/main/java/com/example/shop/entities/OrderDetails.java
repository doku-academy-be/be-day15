package com.example.shop.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="order_details")
@Getter
@Setter
public class OrderDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "order_details_products",
            joinColumns = { @JoinColumn(name = "order_details_id") },
            inverseJoinColumns = { @JoinColumn(name = "products_id") })
    private Set<Products> products = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "order_details_orders",
            joinColumns = { @JoinColumn(name = "order_details_id") },
            inverseJoinColumns = { @JoinColumn(name = "orders_id") })
    private Set<Orders> orders;

    @Column(name="qty")
    private int quantity;

    @Column(name = "amount")
    private double amount;

}
