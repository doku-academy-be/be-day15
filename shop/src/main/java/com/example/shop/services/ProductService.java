package com.example.shop.services;

import com.example.shop.entities.Products;
import com.example.shop.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public Products createProduct(Products product) {
        return productRepository.save(product);
    }

    public List<Products> createProductList(List<Products> list) {
        return productRepository.saveAll(list);
    }

    public List<Products> getProductList() {

        return productRepository.findAll();
    }

    public Products getProductById(Long id) {
        return productRepository.findById(id).orElse(null);
    }

    public Products updateProductById(Products product) {
        Optional<Products> productFound = productRepository.findById(product.getId());

        if (productFound.isPresent()) {
            Products userUpdate = productFound.get();
            userUpdate.setName(product.getName());
            userUpdate.setPrice(product.getPrice());
            userUpdate.setStock(product.getStock());

            return productRepository.save(product);
        } else {
            return null;
        }
    }

    public String deleteProductById(Long id) {
        productRepository.deleteById(id);
        return "User "+ id +" deleted";
    }
}
