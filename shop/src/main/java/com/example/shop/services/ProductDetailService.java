package com.example.shop.services;

import com.example.shop.entities.ProductDetails;
import com.example.shop.repositories.ProductDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductDetailService {

    @Autowired
    private ProductDetailRepository detailRepository;

    public ProductDetails createPD(ProductDetails productDetails) {
        return detailRepository.save(productDetails);
    }

    public List<ProductDetails> createPDList(List<ProductDetails> list) {
        return detailRepository.saveAll(list);
    }

    public List<ProductDetails> getPDList() {
        return detailRepository.findAll();
    }

    public ProductDetails getPDById(Long id) {
        return detailRepository.findById(id).orElse(null);
    }

    public ProductDetails updatePDById(ProductDetails productDetails) {
        Optional<ProductDetails> userFound = detailRepository.findById(productDetails.getId());

        if (userFound.isPresent()) {
            ProductDetails userUpdate = userFound.get();
            userUpdate.setProducts(productDetails.getProducts());
            userUpdate.setDescription(productDetails.getDescription());
            return detailRepository.save(productDetails);
        } else {
            return null;
        }
    }

    public String deletePDById(Long id) {
        detailRepository.deleteById(id);
        return "User "+ id +" deleted";
    }
}
