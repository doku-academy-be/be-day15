package com.example.shop.services;

import com.example.shop.entities.Users;
import com.example.shop.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public Users createUser(Users user) {
        return userRepository.save(user);
    }

    public List<Users> createUserList(List<Users> list) {
        return userRepository.saveAll(list);
    }

    public List<Users> getUserList() {
        return userRepository.findAll();
    }

    public Users getUserById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    public Users updateUserById(Users user) {
        Optional<Users> userFound = userRepository.findById(user.getId());

        if (userFound.isPresent()) {
            Users userUpdate = userFound.get();
            userUpdate.setName(user.getName());
            userUpdate.setEmail(user.getEmail());
            return userRepository.save(user);
        } else {
            return null;
        }
    }

    public String deleteUserById(Long id) {
        userRepository.deleteById(id);
        return "User "+ id +" deleted";
    }
}
