package com.example.shop.services;

import com.example.shop.entities.Orders;
import com.example.shop.entities.ProductDetails;
import com.example.shop.repositories.OrderRepository;
import com.example.shop.repositories.ProductDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

public class OrderService {
    @Autowired
    private OrderRepository detailRepository;

    public Orders createOrder(Orders order) {
        return detailRepository.save(order);
    }

    public List<Orders> createOrderList(List<Orders> list) {
        return detailRepository.saveAll(list);
    }

    public List<Orders> getOrderList() {
        return detailRepository.findAll();
    }

    public Orders getOrderById(Long id) {
        return detailRepository.findById(id).orElse(null);
    }

    public Orders updatePDById(Orders order) {
        Optional<Orders> orderFound = detailRepository.findById(order.getId());

        if (orderFound.isPresent()) {
            Orders orderUpdate = orderFound.get();
//            orderUpdate.setUsers(order.getUsers());
            orderUpdate.setTotalAmount(order.getTotalAmount());
            orderUpdate.setTotalStock(order.getTotalStock());
            orderUpdate.setPaymentType(order.getPaymentType());
            orderUpdate.setPaymentStatus(order.getPaymentStatus());
            return detailRepository.save(order);
        } else {
            return null;
        }
    }

    public String deleteOrderById(Long id) {
        detailRepository.deleteById(id);
        return "User "+ id +" deleted";
    }
}
