package com.example.shop.services;

import com.example.shop.entities.OrderDetails;
import com.example.shop.entities.ProductDetails;
import com.example.shop.repositories.OrderDetailRepository;
import com.example.shop.repositories.ProductDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderDetailService {
    @Autowired
    private OrderDetailRepository detailRepository;

    public OrderDetails createOD(OrderDetails orderDetails) {
        return detailRepository.save(orderDetails);
    }

    public List<OrderDetails> createODList(List<OrderDetails> list) {
        return detailRepository.saveAll(list);
    }

    public List<OrderDetails> getODList() {
        return detailRepository.findAll();
    }

    public OrderDetails getODById(Long id) {
        return detailRepository.findById(id).orElse(null);
    }

    public OrderDetails updateODById(OrderDetails orderDetails) {
        Optional<OrderDetails> orderDFound = detailRepository.findById(orderDetails.getId());

        if (orderDFound.isPresent()) {
            OrderDetails orderDUpdate = orderDFound.get();
            orderDUpdate.setProducts(orderDUpdate.getProducts());
            orderDUpdate.setOrders(orderDUpdate.getOrders());

            orderDUpdate.setAmount(orderDUpdate.getAmount());
            orderDUpdate.setQuantity(orderDUpdate.getQuantity());
            return detailRepository.save(orderDetails);
        } else {
            return null;
        }
    }

    public String deleteODById(Long id) {
        detailRepository.deleteById(id);
        return "User "+ id +" deleted";
    }
}
